FROM alpine:latest
RUN apk --no-cache add bash curl
RUN curl -Lo /usr/bin/hadolint https://github.com/hadolint/hadolint/releases/latest/download/hadolint-Linux-x86_64 && \
    chmod +x /usr/bin/hadolint
WORKDIR /app
COPY ./Dockerfile /app/
CMD ["hadolint", "Dockerfile"]
